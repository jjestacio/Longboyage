import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  fetch,
  Button,
  View
} from 'react-native';

export default class EndTrip extends Component {
	constructor() {
		super();

		this.state = {
			totalTime: '0:00',
			avgSpeed: '0.00',
			totalDistance: '0.00',
			stopTrip: true,
		};
	}

	render() {
		const { totalTime, avgSpeed, totalDistance, stopTrip } = this.state;
		const { changePage } = this.props;

		return (
			<View style={styles.container}>
				<Text style={styles.title}>Trip Summary</Text>
				<Text style={styles.time}> { totalTime } min</Text>
				<Text style={styles.speed}> { avgSpeed } mph</Text>
				<Text style={styles.distance}>{ totalDistance } miles</Text>
				<Button 
					title="Return Home"
					color="#841584"
					onPress={() => changePage('HOME')}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
  	fontSize: 50,
  	textAlign: 'center',
  	marginTop: 0,
  	marginBottom: 30,
  },
  time: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10,
  },
  speed: {
	fontSize: 40,
    textAlign: 'center',
    margin: 10,
  },
  distance: {
  	fontSize: 40,
  	textAlign: 'center',
  	marginTop: 10,
  	marginBottom: 20,
  }

});