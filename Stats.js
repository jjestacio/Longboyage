import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  fetch,
  Button,
  View
} from 'react-native';

export default class Stats extends Component {
	constructor() {
		super();

		this.state = {
			speed: '0.00 miles/hour',
			time: '0:00',
			distance: '0.00 miles',
			stopTrip: false,
		};
	}

	render() {
		const { speed, time, distance, stopTrip } = this.state;
		const { changePage } = this.props;

		return (
			<View style={styles.container}>
				<Text style={styles.time}>{ time }</Text>
				<Text style={styles.speed}>{ speed }</Text>
				<Text style={styles.distance}>{ distance }</Text>
				<Button 
					title="Stop Trip"
					color="#FF0000"
					onPress={() => changePage('END_TRIP_PAGE')}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  time: {
  	textAlign: 'center',
  	fontSize: 100,
  	marginBottom: 20,
  },
  speed: {
  	textAlign: 'center',
  	fontSize: 30,
  	margin: 20,
  },
  distance: {
  	textAlign: 'center',
  	fontSize: 30,
  	marginTop: 20,
  	marginBottom: 40,
  }

});