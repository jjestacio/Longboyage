/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View
} from 'react-native';

import EndTrip from './EndTrip.js'
import Stats from './Stats.js'

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      displayPage: 'HOME'
    };

    this.changePage = this.changePage.bind(this);
  }

  changePage(displayPage) {
    this.setState({ displayPage });
  }

  toggleStats() {
    this.setState({ displayStats: 'STATS_PAGE' });
  }

  render() {
    const { displayPage } = this.state;

    if (displayPage == 'END_TRIP_PAGE')
    return (
      <EndTrip changePage={displayPage => this.changePage(displayPage)} />
    );

    if (displayPage == 'STATS_PAGE')
    return (
      <Stats changePage={displayPage => this.changePage(displayPage)} />
    );

    return (
      <View style={styles.container}>
        <Button 
          title="Start Trip"
          color="#009bf0"
          onPress={() => this.changePage('STATS_PAGE')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('App', () => App);
